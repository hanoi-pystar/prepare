## Cài bộ Anaconda

Funny: Anaconda hình như là "siêu trăn" thì phải.
Đúng như tên gọi của nó, Anaconda chứa phần lớn các thư viện "hịn" nhất của Python, người dùng gần như sẽ không phải cài thêm thư viện nữa
- Thích hợp cho việc học & làm việc ở môi trường offline (có policy chặn hết các loại kết nối internet)
- Thích hợp cho cả những "tay ngang" muốn học Python để tối ưu hóa công việc của mình.
- Anaconda hỗ trợ tối đa datasciense, rất nhiều video training từ bootcamp được anaconda cung cấp miễn phí.

**Download**
[https://www.anaconda.com/download/](https://www.anaconda.com/download/)

Có đầy đủ các bản cho Window/Linux/MacOS. 
Bản nào cũng có cả hướng dẫn install luôn
- Linux: [https://docs.anaconda.com/anaconda/install/linux](https://docs.anaconda.com/anaconda/install/linux)
- Window: [https://docs.anaconda.com/anaconda/install/windows](https://docs.anaconda.com/anaconda/install/windows)
Lưu ý: Phải tick vào dấu "add path" khi cài đặt anaconda nếu không sẽ không sử dụng được.
- MacOS: [https://docs.anaconda.com/anaconda/install/mac-os](https://docs.anaconda.com/anaconda/install/mac-os)



<hr>
P/S: Nếu bạn có khó khăn trong việc tự học Python hoặc muốn học Python nhanh hơn, đừng ngại, hãy liên lạc với tôi qua inbox Facebook: [![](img/facebook-icon.png)](https://www.facebook.com/nguyen.dev.1213)
<hr>

[Task list](Lo-trinh-hoc-Python.md)