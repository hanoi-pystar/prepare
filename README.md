# Chuẩn bị trước buổi học đầu tiên


## Cài đặt Python: (chọn 1 trong 3, nên chọn 3)
    
Trên trang chủ của Python hiện tại đang có 2 version để sử dụng: Python 2 và Python3. 
Đọc News thì thấy Python2 đến năm 2020 sẽ không hỗ trợ nữa.
Các hãng cung cấp thứ 3 và các tác giả của các LIB của Python hiện tại phần lớn đã hỗ trợ Python3 (đến 11/2017 là 324/360 libs).
Vì vậy, với những người mới học, nên chọn Python3


[Cài đặt Python "thuần"](Cai-dat-Python-phan1.md)

[Cài đặt WinPython](Cai-dat-Python-phan2.md)

[Cài đặt bộ Anaconda](Cai-dat-Python-phan3.md)


##  Gõ những dòng lệnh Python đầu tiên

 **Kiểm tra kết quả cài đặt.**

Sau khi cài đặt xong, mở terminal/cmd và gõ:
```
python --version
```
(nếu sử dụng winpython thì dùng terminal có trong thư mục giải nén)
Nếu hiển thị ra Python phiên bản bao nhiêu --> đã cài đặt thành công.

**Gõ dòng lệnh đầu tiên.**

Sau khi cài đặt xong, tận hưởng thành quả của mình một chút nhỉ :)

Trên terminal/cmd, gõ:

```
python
```

Màn hình sẽ ra dấu nhắc >>> để chúng ta gõ lệnh, phía trên là các thông tin về phiên bản của python đã được cài đặt và chọn làm mặc định.

Tiếp tục gõ

```
print("Hello Python")
```

Màn hình ra kết quả: Hello Python

Vậy là bạn đã thành công với chương trình đầu tiên bằng Python.

Để thoát ra khỏi màn hình chờ nhập lệnh của Python, hãy gõ 

```
exit()
```


Note: 
Nếu màn hình không ra mà hiện ra Exception hoặc Error thì sao ? --> Hãy nhìn lên phiên bản, có lẽ bạn đang ở Python2 (nếu sử dụng ubuntu thì gần như sẽ gặp lỗi này :D).

Hãy thoát khỏi màn hình nhập lệnh và quay trở về màn hình terminal/cmd và gõ:

```
Python3
```


## Cài đặt editor:

 Cài đặt 2 phần mềm sau:

[Jupyter notebook](Huong-dan-cai-dat-va-su-dung-jupyter-notebook.md)

[Visual studio code](Huong-dan-cai-dat-cau-hinh-visual-studio-code.md)

Tham khảo:

[Lựa chọn editor nào ?](Editor-cho-Python-Opps-qua-nhieu-lua-chon.md)

## Thư viện tham khảo:

Vào trang chủ của python documents và chọn download tài liệu dạng HTML: [Link download](https://docs.python.org/3/download.html)

![Dowload docs](img/Python_Docs.png)





<hr>
P/S: Nếu bạn có khó khăn trong việc tự học Python hoặc muốn học Python nhanh hơn, đừng ngại, hãy liên lạc với tôi qua inbox Facebook: [![](img/facebook-icon.png)](https://www.facebook.com/nguyen.dev.1213)
<hr>

[Task list](Lo-trinh-hoc-Python.md)