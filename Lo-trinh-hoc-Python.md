# Lộ trình học Python:
(update cho các nhóm tháng 12 - 2017)

**Ghi chú:** Trẻ em từ 8 tuổi đã có thể học được lập trình Python (biết tính toán + - * / là bắt đầu học được), không có lý do gì mà các anh/chị đã học xong THPT lại không học được Python.

**Thuật toán:** Thuật toán là thứ cao siêu chỉ dành cho các chuyên gia, các ông đầu to ? Không.
Thuật toán chỉ là một cái tên do con người đặt ra và cơ số người khác "huyễn hoặc" nó lên. Thuật toán ở quanh ta, nó nằm sẵn trong tư duy của mỗi người. Nó giống như hôm nay bạn đi học, đi làm trẽ 5', ngày mai bạn sẽ đi sớm hơn 5' vậy.
--> Nếu ai bảo không giỏi toán thì không nên học Python hay bất kể ngôn ngữ lập trình nào khác. **Đừng tin!** Ai cũng có thể học và ứng dụng được Python.


## Buổi 1: 
- Làm quen, xác định mục đích học.
    Nếu học để làm Data mining (AI, DS, ML): Bỏ qua những thứ liên quan đến web dev
- Giới thiệu về mentor
- Giới thiệu về Python: Sức mạnh của Python, cách phát âm chuẩn,...
- Basic data type: 
    - What is object ?
    - Number (integer, float, complex): Thực hiện xây dựng một calculator bằng Python

- Bài tập: Các bài tập tính toán dựa trên kiểu dữ liệu number
(10 ex)

Note: Giới thiệu qua về control flow và toward looping

## Buổi 2: 
Tập trung tìm hiểu về String:
- Init, Assig...
- Built-in method:
    - Ý nghĩa
    - Cấu trúc
    - Tham số vào/ra
    - ví dụ: Mỗi phương thức thực hiện demo 3-4 ví dụ.
- Bài tập: Xử lý bài tập liên quan đến áp dụng các phương thức của string. 
(10 ex)

Note: Giới thiệu qua về list, for looping

## Buổi 3: 
Tập trung tìm hiểu về các kiểu dữ liệu list, set, tuple
- Init, Assign...
- Built-in method
    - Ý nghĩa
    - Cấu trúc
    - Tham số vào/ra
    - ví dụ: Mỗi phương thức thực hiện demo 3-4 ví dụ.
- For looping:
- control flow by if/else
- Bài tập: Xử lý bài tập liên quan đến áp dụng các phương thức của list và looping. 
(40 ex)

## Buổi 4:
- Dictonary, Datetime, Boolean advance, controlflow advance, I/O simple


## Buổi 5:
- I/O advance, JSON, XML
- OOP with Python: Module, Class, Function


## Buổi 6:
- T-SQL simple
- Python with DBMS (hệ quản trị cơ sở dữ liệu)


## Buổi > 6: Python advance
Thực hiện dạy theo các chủ đề đã thảo luận từ buổi 1.
Ví dụ: 
- Data science: Numpy, pandas,...
- Webdev: Flask, Django, Jinja2,...
- Crawl/Scrap: Html overview, requests, bs4,...
