
## Cài đặt Python.

### Cài đặt python mặc định


**Trên Ubuntu**

Kiểm tra version (mặc định có Python2): 
```
python --version
```

Cài đặt từ Repo:
```
sudo apt update

sudo apt install python3.6
```

Kiểm tra kết quả cài đặt
```
python --version
```

Ngoài ra còn cách install qua PPA, install từ package được download từ trang chủ
[https://www.python.org/downloads/source/](https://www.python.org/downloads/source/)

**Trên Window**

Vào link này để download bản Python 3.6.x mới nhất:
[https://www.python.org/downloads/](https://www.python.org/downloads/)

Install như các phần mềm thông thường. Nhớ chọn "Add path" ở bước đầu tiên.



Mặc định Python có rất ít các package. Sau này, khi dùng đến package nào thì mới install qua Pip. --> Lựa chọn tối ưu cho các lập trình viên chuyên nghiệp.







<hr>
P/S: Nếu bạn có khó khăn trong việc tự học Python hoặc muốn học Python nhanh hơn, đừng ngại, hãy liên lạc với tôi qua inbox Facebook: [![](img/facebook-icon.png)](https://www.facebook.com/nguyen.dev.1213)
<hr>

[Task list](Lo-trinh-hoc-Python.md)